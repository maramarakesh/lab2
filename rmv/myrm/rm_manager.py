#!/usr/bin/env python
# -*- coding: utf-8 -*-


import argparse
import os
import progressbar
import time
import myrm.rm as rm
import logging
import myrm.trash as trash
import myrm.exceptions2 as exceptions2


logger = logging.getLogger('application')
bar = progressbar.ProgressBar().start()

def initializeLogger(force, silent):

	logger.setLevel(logging.DEBUG)
	fh = logging.FileHandler(os.path.join(os.path.expanduser('~'), 'lab2/rmlogs.log'))
	ch = logging.StreamHandler()

	fh.setLevel(logging.CRITICAL) if silent else fh.setLevel(logging.DEBUG)
	ch.setLevel(logging.CRITICAL) if silent or force else ch.setLevel(logging.INFO)

	fileFormatter = logging.Formatter(fmt='# %(asctime)s\n %(levelname)s: %(module)s.%(funcName)s - %(message)s',
									  datefmt='%Y-%m-%d, %H:%M:%S')
	consoleFormatter = logging.Formatter('%(levelname)s:  %(message)s')
	fh.setFormatter(fileFormatter)
	ch.setFormatter(consoleFormatter)

	logger.addHandler(fh)
	logger.addHandler(ch)


def parse_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('-r', '--regex', action='store_true', help='remove files according regular expression')
	parser.add_argument('-f', '--force', action='store_true', help='ignore all errors')
	parser.add_argument('-i', '--interactive', action='store_true', help='answer user before removing')
	parser.add_argument('-s', '--silent', action='store_true', help='runprogram with silent mode')
	parser.add_argument('-l', '--link', action='store_true', help='follow symlinks')
	parser.add_argument('--dryrun', action='store_true', help='run program with dry-run mode')
	parser.add_argument('--show', action='store_const', const=True, default=False, help='show trash content')
	parser.add_argument('paths', nargs='*', help='path to folder or file', type=str)
	args = parser.parse_args()
	return args

def process_request(args):
	rmtrash = trash.Trash(args.interactive, args.force, args.silent,
				  args.link, args.dryrun)

	if args.show:
		rmtrash.show()
		exit(0)

	targets = 0
	for path in args.paths:
		if args.regex:
				rmtrash.get_files_reg(path)
				exit(0)

		if os.path.isfile(path):
			rmtrash.get_file(path)

		elif os.path.isdir(path):
			rmtrash.get_dir(path)

		elif os.path.islink(path):
			rmtrash.get_link(path)
		else:
			logger.error("SourceError: file or directory \'%s\' does not exist",
					 	 os.path.basename(path))
			if args.force:
				continue
			raise exceptions2.SourceError()

		targets += 1
		bar.update((targets * 100) / len(args.paths))
		time.sleep(0.3)

	bar.finish()

def Main():
	args = parse_arguments()

	initializeLogger(args.force, args.silent)

	try:
		process_request(args)
	except exceptions2.SourceError:
		exit(1)
	except exceptions2.NotAccessError:
		exit(1)
	except exceptions2.NotFoundError:
		exit(1)
	except exceptions2.SymlinkError:
		exit(1)

if __name__ == '__main__':
	Main()

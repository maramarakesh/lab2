#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import myrm.rm as rm
import json
import logging
import myrm.exceptions2 as exceptions2
import ConfigParser

from os.path import join

class Config(object):

	def __init__(self, json_format=True, test=False):

		self.standart_configs = {"policy": "full",
		 					"path": join(os.path.expanduser("~"), "/lab2/trash"),
							"capacity": 250}
		self.configs = ConfigParser.RawConfigParser()

		if not test:
			self.create_configs()

		self.configs_json = json.load(open(join(os.path.expanduser("~"), 'lab2/rmconfigs.json'), 'r'))
		self.configs.read(join(os.path.expanduser("~"), 'lab2/rmconfigs.cfg'))

		if json_format:
			self.policy = self.configs_json['policy']
			self.capacity = self.configs_json['capacity']
			self.path = self.configs_json['path']
		else:
			self.capacity = self.configs.get('CONFIGS', 'Capacity')
			self.path = self.configs.get('CONFIGS', 'Path')
			self.policy = self.configs.get('CONFIGS', 'Policy')

	def create_configs(self):
		if not os.path.exists(join(os.path.expanduser("~"), 'lab2/rmconfigs.json')):
			json.dump(self.standart_configs, open(join(os.path.expanduser("~"), 'lab2/rmconfigs.json'), 'w'))
		if not os.path.exists(join(os.path.expanduser("~"), 'lab2/rmconfig.cfg')):
			self.configs.add_section('CONFIGS')
			self.configs.set('CONFIGS', 'Capacity', 250)
			self.configs.set('CONFIGS', 'Path', join(os.path.expanduser("~"), 'lab2/trash'))
			self.configs.set('CONFIGS', 'Policy', 'full')
			with open(join(os.path.expanduser("~"), 'lab2/rmconfigs.cfg'), 'w') as configfile:
				self.configs.write(configfile)


	def change_capacity(self, new_capacity):
		if int(new_capacity) < 0:
			logger.error("ConfigError: configurations have not been changed, %d mb is wrong capacity",
						 int(new_capacity))
			raise exceptions2.ConfigError()

		self.capacity = new_capacity
		self.write_configs(None, None, new_capacity)

		logger.info("trash capacity has been changed on %d mb",
					int(new_capacity))


	def change_policy(self, policy):
		if (policy != 'time' and policy != 'big'
		   and policy != 'full'):
			logger.error("ConfigError: configurations have not been changed, \'%s\' is wrong policy",
						 args.configuration)
			raise exceptions2.ConfigError()

		self.policy = policy
		self.write_configs(None, self.policy, None)

		logger.info("cleaning policy has been changed on \'%s\'", policy)


	def change_path(self, new_path):
		if not os.path.exists(new_path):
			logger.error("ConfigError: configurations have not been changed, \'%s\' is wrong path",
						 os.path.abspath(os.path.join(new_path, 'trash')))
			raise exceptions2.ConfigError()

		if self.path == new_path:
			return

		try:
			rm.rpdir(os.path.abspath(self.path), os.path.join(os.path.abspath(new_path), 'trash'))
		except OSError as error:
			print error
			return

		self.write_configs(new_path, None, None)

		logger.info("trash location has been changed successfully \'%s/trash\'", os.path.abspath(new_path))


	def path(self):
		return self.path

	def policy(self):
		return self.policy

	def capacity(self):
		return int(self.capacity)


	def write_configs(self, new_path, new_policy, new_capacity):
		if new_path != None:
			self.configs.set('CONFIGS', 'Path', os.path.abspath(new_path) + '/trash')
			self.configs_json['path'] = os.path.join(new_path, 'trash')
		if new_policy != None:
			self.configs.set('CONFIGS', 'Policy', new_policy)
			self.configs_json['policy'] = new_policy;
		if new_capacity != None:
			self.configs.set('CONFIGS', 'Capacity', new_capacity)
			self.configs_json['capacity'] = new_capacity;
		with open(join(os.path.expanduser("~"), 'lab2/rmconfigs.cfg'), 'w') as configfile:
				self.configs.write(configfile)

		json.dump(self.configs_json, open(join(os.path.expanduser("~"), 'lab2/rmconfigs.json'), 'w'))


logger = logging.getLogger('config')

def initializeLogger(silent):

	logger.setLevel(logging.DEBUG)
	fh = logging.FileHandler(join(os.path.expanduser("~"), 'lab2/rmlogs.log'))
	ch = logging.StreamHandler()

	fh.setLevel(logging.CRITICAL) if silent else fh.setLevel(logging.DEBUG)
	ch.setLevel(logging.CRITICAL) if silent else ch.setLevel(logging.INFO)

	fileFormatter = logging.Formatter(fmt='# %(asctime)s\n %(levelname)s: %(module)s.%(funcName)s[%(lineno)s] - %(message)s',
									  datefmt='%Y-%m-%d, %H:%M:%S')
	consoleFormatter = logging.Formatter('%(levelname)s:  %(message)s')
	fh.setFormatter(fileFormatter)
	ch.setFormatter(consoleFormatter)

	logger.addHandler(fh)
	logger.addHandler(ch)


def parse_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('--json', action='store_true', help='json format for configuration fileS')
	parser.add_argument('-p','--path', action='store_true', help='path to trash')
	parser.add_argument('-c','--capacity', action='store_true', help='max trash capacity in megabytes')
	parser.add_argument('-l', '--policy', action='store_true', help='time - remove old files from trash for adding new'
									   'big - remove big files from trash for adding new'
									   'full - remove all files from trash for adding new')
	parser.add_argument('-s', '--silent', action='store_true', help='runprogram with silent mode')
	parser.add_argument('configuration', help = 'configuration', type=str)
	args = parser.parse_args()
	return args


def Main():
	args = parse_arguments()
	initializeLogger(args.silent)

	config = Config(json_format=True, test=False)

	try:
		if args.path:
			config.change_path(args.configuration)
		elif args.capacity:
			config.change_capacity(args.configuration)
		elif args.policy:
			config.change_policy(args.configuration)
	except exceptions2.ConfigError:
		exit(1)


if __name__ == '__main__':
	Main()

#!/usr/bin/env python 
# -*- coding: utf-8 -*-


import argparse
import os
import rmv.rm as rm
import logging
import rmv.trash as trash
import rmv.exceptions2 as exceptions2

	
logger = logging.getLogger('application')

def initializeLogger(force, silent):

	logger.setLevel(logging.DEBUG)
	fh = logging.FileHandler('/usr/logs/rmlogs.log')
	ch = logging.StreamHandler()

	fh.setLevel(logging.CRITICAL) if silent else fh.setLevel(logging.DEBUG)
	ch.setLevel(logging.CRITICAL) if silent or force else ch.setLevel(logging.INFO)

	fileFormatter = logging.Formatter(fmt='# %(asctime)s\n %(levelname)s: %(module)s.%(funcName)s - %(message)s',
									  datefmt='%Y-%m-%d, %H:%M:%S')
	consoleFormatter = logging.Formatter('%(levelname)s:  %(message)s')
	fh.setFormatter(fileFormatter)
	ch.setFormatter(consoleFormatter)

	logger.addHandler(fh)
	logger.addHandler(ch)


def parse_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('-d', '--delete', action='store_true', help='remove file or directory without adding to trash')
	parser.add_argument('-r', '--regex', action='store_true', help='remove files according regular expression')
	parser.add_argument('-f', '--force', action='store_true', help='ignore all errors')
	parser.add_argument('-i', '--interactive', action='store_true', help='answer user before removing')
	parser.add_argument('-s', '--silent', action='store_true', help='runprogram with silent mode')
	parser.add_argument('-l', '--link', action='store_true', help='follow symlinks')
	parser.add_argument('--dryrun', action='store_true', help='run program with dry-run mode')
	parser.add_argument('paths', nargs='+', help='path to folder or file', type=str)
	args = parser.parse_args()
	return args

def process_request(args):
	rmtrash = trash.Trash(args.interactive, args.force, args.silent,
				  args.link, args.dryrun)

	for path in args.paths:
		if args.regex:
				(rm.rmregex(path, args.interactive and not args.silent,
				 args.force, args.dryrun)
				if args.delete else rmtrash.get_files_reg(path))
				continue

		if os.path.isfile(path):
				(rm.rmfile(path, args.interactive and not args.silent,
				 args.force, args.dryrun)
				if args.delete else rmtrash.get_file(path))

		elif os.path.isdir(path):
				(rm.rmdir(path, args.interactive and not args.silent,
				 args.force, args.dryrun)
				if args.delete else rmtrash.get_dir(path))

		elif os.path.islink(path):
				(rm.rmlink(path, args.interactive and not args.silent,
				 args.force, args.dryrun, args.link)
				if args.delete else rmtrash.get_link(path))
		else:
			logger.error("SourceError: file or directory \'%s\' does not exist", 
					 	 os.path.basename(path))
			if args.force:
				continue
			raise exceptions2.SourceError()



def Main():
	args = parse_arguments()

	initializeLogger(args.force, args.silent)

	try:
		process_request(args)
	except exceptions2.SourceError:
		exit(1)
	except exceptions2.NotAccessError:
		exit(1)
	except exceptions2.NotFoundError:
		exit(1)
	except exceptions2.SymlinkError:
		exit(1)

if __name__ == '__main__':
	Main()
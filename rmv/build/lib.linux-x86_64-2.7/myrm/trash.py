# -*- coding: utf-8 -*-

import os
import json
import progressbar
import time
import myrm.rm as rm
import myrm.json_manager as json_manager
import logging
import myrm.exceptions2 as exceptions2
import myrm.ask_user as ask
import myrm.config as config

class Trash(object):

	def __init__(self, interactive, force, silent, symlink, dryrun):

		self.interactive = interactive
		self.force = force
		self.silent = silent
		self.symlink = symlink
		self.dryrun = dryrun

		self.logger = logging.getLogger('application.trash')

		self.configs = config.Config()

		self.max_capacity = self.configs.capacity
		self.policy = self.configs.policy
		self.path = self.configs.path
		self.current_capacity = self._count_capacity(self.path)

		if not os.path.exists(self.path):
			os.mkdir(self.path)
			f = open(os.path.join(self.path, 'content.json'), 'w')

		json_manager.clear_trash_content(self.path)


	def path(self):
		return self.path

	def capacity(self):
		return self.current_capacity

	def _count_capacity(self, source):
		capacity = 0.0
		for root, dirs, files in os.walk(source):
			for file in files:
				capacity += os.path.getsize(os.path.join(root, file))
		return capacity / 1000000


	# Show trash content
	def show(self):
		names = os.listdir(self.path)

		count_names = len(names)
		while count_names > 0:
			print "{0:^15} {1:^15} {2:^15}".format(names[count_names-1],
									   names[count_names-2],
									   names[count_names-3])
			count_names = count_names - 3
			

	# Restoring tree for file
	def _restore_tree(self, destination):
		tree = os.path.dirname(destination)

		if not self.dryrun:
			os.makedirs(tree)

		self.logger.info('tree \'%s\' was restored', tree)


	def get_link(self, source):
		name = os.path.basename(source)
		if not self.symlink:
			self.get_file(source)
			return

		realpath = os.path.realpath(source)

		if os.path.isfile(realpath):
			self.get_file(realpath)
		elif os.path.isdir(realpath):
			self.get_dir(realpath)
		else:
			self.logger.error('SymlinkError: symlink \'%s\' is not active',
							  os.path.basename(source))
			if not self.force:
				raise exceptions2.SymlinkError()



	def get_file(self, source):
		name = os.path.basename(source)
		if (self.interactive and not self.force and not self.silent and not
		    ask.AskUser.ask_user("rm: do you want to move the file to the trash \'%s\' ?" %name)):
			return

		if not os.access(source, os.W_OK):
			self.logger.error('NotAccessError: no access rights for file \'%s\'',
							  name)
			if not self.force:
				raise exceptions2.NotFoundError()
			return

		try:
			self._check_capacity(source)
		except NotEnoughMemoryError:
			if self.force:
				return
			exit(1)

		if not self.dryrun:
			endname = rm.rpfile(source, os.path.join(self.path, name))
			json_manager.add_trash_content(endname, source)

		self.logger.info("file \'%s\' was moved in trash successfull", name)


	def get_dir(self, source):
		name = os.path.basename(source)
		if (self.interactive and not self.force and not self.silent and not
		    ask.AskUser.ask_user("rm: do you want to move the directory to the trash \'%s\' ?" %name)):
			return

		if not os.access(source, os.W_OK):
			self.logger.error('NotAccessError: no access rights for directory \'%s\'',
							  name)
			if not self.force:
				raise exceptions2.NotFoundError()
			return

		try:
			self._check_capacity(source)
		except NotEnoughMemoryError:
			if self.force:
				return
			exit(1)

		if not self.dryrun:
			endname = rm.rpdir(source, os.path.join(self.path, name))
			json_manager.add_trash_content(endname, source)

		self.logger.info("directory \'%s\' was moved in trash successfully",
						 name)


	def get_files_reg(self, source):
		files = []
		bar = progressbar.ProgressBar().start()

		try:
			files = rm.find_files_regex(source)
		except exceptions2.SourceError:
			if self.force:
				return
			exit(1)

		except exceptions2.NotFoundError:
			if self.force:
				return
			exit(1)

		targets = 0
		for file in files:
			targets += 1
			self.get_file(file)
			bar.update((targets * 100) / len(files))
			time.sleep(0.3)
		bar.finish()


	def _check_capacity(self, source):
		if os.path.isfile(source):
			size_obj = os.path.getsize(source) / 1000000.0
		else:
			size_obj = self._count_capacity(source)

		if size_obj > self.max_capacity:
			self.logger.error('NotEnoughMemoryError: the limit of memory is exceeded, current capacity = %f, max capacity = %f',
					  		  self.current_capacity, self.max_capacity)
			raise exceptions2.NotEnoughMemoryError()

		elif size_obj + self.current_capacity > self.max_capacity:
			space = size_obj + self.current_capacity - self.max_capacity
			rm.rmpolicy(self.path, self.policy, space, self.interactive and not self.force, self.dryrun)


	def restore_file(self, name):
		basename = os.path.basename(name)
		if (self.interactive and not self.force and not self.silent and not
		    ask.AskUser.ask_user("rm: do you want to restore file \'%s\' ?" %basename)):
			return

		oldpath = json_manager.find_trash_content(basename)

		if oldpath == None:
			self.logger.error('RestoreError: cannot restore file \'%s\', data corrupted!',
							  basename)
			if not self.force:
				raise exceptions2.RestoreError()

		if os.path.exists(os.path.dirname(oldpath)):
			if not self.dryrun:
				rm.rpfile(name, os.path.join(os.path.dirname(oldpath), basename))
		else:
			self._restore_tree(oldpath)
			if not self.dryrun:
				rm.rpfile(name, oldpath)

		self.logger.info("file \'%s\' was restored successfully", basename)


	def restore_dir(self, name):
		basename = os.path.basename(name)
		if (self.interactive and not self.force and not self.silent and not
			ask.AskUser.ask_user("rm: do you want to restore directory \'%s\' ?" %basename)):
			return

		oldpath = json_manager.find_trash_content(basename)

		if oldpath == None:
			self.logger.error('RestoreError: cannot restore diretory \'%s\', data corrupted!',
							  name)
			if not self.force:
				raise exceptions2.RestoreError()

		if os.path.exists(os.path.dirname(oldpath)):
			if not self.dryrun:
				rm.rpdir(name, oldpath)
		else:
			self._restore_tree(oldpath)
			if not self.dryrun:
				rm.rpdir(name, oldpath)

		self.logger.info("directory \'%s\' was restored successfull", basename)

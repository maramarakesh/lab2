#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import logging
import progressbar
import time
import myrm.trash as trash
import myrm.exceptions2 as exceptions2

logger = logging.getLogger('application')
bar = progressbar.ProgressBar().start()

def initializeLogger(force, silent):

	logger.setLevel(logging.DEBUG)
	fh = logging.FileHandler(os.path.join(os.path.expanduser('~'), 'lab2/rmlogs.log'))
	ch = logging.StreamHandler()

	fh.setLevel(logging.CRITICAL) if silent else fh.setLevel(logging.DEBUG)
	ch.setLevel(logging.CRITICAL) if silent or force else ch.setLevel(logging.INFO)

	fileFormatter = logging.Formatter(fmt='# %(asctime)s\n %(levelname)s: %(module)s.%(funcName)s[%(lineno)s] - %(message)s',
									  datefmt='%Y-%m-%d, %H:%M:%S')
	consoleFormatter = logging.Formatter('%(levelname)s:  %(message)s')
	fh.setFormatter(fileFormatter)
	ch.setFormatter(consoleFormatter)

	logger.addHandler(fh)
	logger.addHandler(ch)


def parse_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('names', nargs='+', help='name of folder or file to restore', type=str)
	parser.add_argument('-f', '--force', action='store_true', help='ignore all errors')
	parser.add_argument('-i', '--interactive', action='store_true', help='answer user before removing')
	parser.add_argument('-s', '--silent', action='store_true', help='run program with silent mode')
	parser.add_argument('--dryrun', action='store_true', help='run program with dry-run mode')
	args = parser.parse_args()
	return args

def process_request(args):
	rmtrash = trash.Trash(args.interactive, args.force, args.silent, False, args.dryrun)

	targets = 0
	for name in args.names:
		fullname = os.path.join(rmtrash.path, name)

		if os.path.isfile(fullname):
			rmtrash.restore_file(fullname)

		elif os.path.isdir(fullname):
			rmtrash.restore_dir(fullname)

		else:
			logger.error("SourceError: \'%s\' does not exist ", name)
			if args.force:
				continue
			raise exceptions2.SourceError()
		targets += 1
		bar.update((targets * 100) / len(args.names))
		time.sleep(0.3)
	bar.finish()



def Main():
	args = parse_arguments()

	initializeLogger(args.force, args.silent)

	try:
		process_request(args)
	except exceptions2.SourceError:
		exit(1)
	except exceptions2.RestoreError:
		exit(1)


if __name__ == '__main__':
	Main()

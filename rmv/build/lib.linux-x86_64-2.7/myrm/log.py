#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import logging
from os.path import expanduser, join

def show():
	logfile = open(join(expanduser("~"), 'lab2/rmlogs.log'), 'r')
	for line in logfile:
		print line.strip()

def Main():
	show()


if __name__ == '__main__':
	Main()
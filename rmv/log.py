#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import logging

def show():
	logfile = open('/usr/logs/rmlogs.log', 'r')
	for line in logfile:
		print line.strip()

def Main():
	show()


if __name__ == '__main__':
	Main()
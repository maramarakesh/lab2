# -*- coding: utf-8 -*- 

import os
from os.path import join
import json
import rmv.config as config

def add_trash_content(name, path):
	if not os.path.exists(content_path):
		open(content_path, 'w')
	try:
		content = json.load(open(content_path, 'r'))
	except ValueError:
		content = {}
	content[name] = os.path.join(os.getcwd(), path)
	json.dump(content, open(content_path, 'w'))	


def find_trash_content(name):
	try:
		content = json.load(open(content_path, 'r'))
	except ValueError:
		content = {}

	try:	
		oldpath = content[name]
		del content[name]
	except KeyError:
		return None

	json.dump(content, open(content_path, 'w'))
	return oldpath


def clear_trash_content(path):
	names = os.listdir(path)
	if not os.path.exists(content_path) or len(names) == 0:
		return

	try:
		content = json.load(open(content_path, 'r'))
	except:
		content = {}

	list = []

	for name in content:
		if name not in names:
			list.insert(0, name)

	for item in list:
		del content[item]

	json.dump(content, open(content_path, 'w'))


configs = config.Config()
trash_path = configs.path
content_path = os.path.join(trash_path, 'content.json')